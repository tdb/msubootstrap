# MSU Bootstrap Theme

The current bootrap version used by this branch is `3.1.1`

## Getting Started

To checkout this repository:

```shell
git clone git@gitlab.msu.edu:ezzomich/msubootstrap.git
cd msubootstrap
git submodule update --init
```

If you've already checked out the repo, and want to update your version, do:

```shell
git fetch origin
git merge origin/master
git submodule update
```

## Using the Bootstrap

If you just want to use the Bootstrap, just copy the files in the `dist` folder into your project.  You can use the `template.html` file as a page template to get you started.

## Development

If you're interested in developing or modifying the MSU Bootstrap, edit the LESS variables in MSUBootstrap.less.  The index.html file is designed to aid in development by giving a preview of the bootstrap elements (Thanks to bootswatch for this markup).

* see http://lesscss.org/ for more information on developing with LESS.

MSUBootstrap uses [Grunt](http://gruntjs.com/) with convenient methods for working with the framework. It's how we compile our code, run tests, and more. To use it, install the required dependencies as directed and then run some Grunt commands.

### Install Grunt

From the command line:

1. Install `grunt-cli` globally with `npm install -g grunt-cli`.
2. Navigate to the root `/MSUBootstrap` directory, then run `npm install`. npm will look at [package.json](https://gitlab.msu.edu/ezzomich/msubootstrap/blob/master/package.json) and automatically install the necessary local dependencies listed there.

When completed, you'll be able to run the various Grunt commands provided from the command line.

**Unfamiliar with npm? Don't have node installed?** That's a-okay. npm stands for [node packaged modules](http://npmjs.org/) and is a way to manage development dependencies through node.js. [Download and install node.js](http://nodejs.org/download/) before proceeding.

### Available Grunt commands

#### Build - `grunt`
Run `grunt` to  compile the CSS, JavaScript, images, and fonts into `/dist`. **Uses [Less](http://lesscss.org/).**

#### Only compile CSS - `grunt dist-css`
`grunt dist-css` compiles the less into css in the `/dist`. **Uses [Less](http://lesscss.org/).**

#### Watch - `grunt watch`
This is a convenience method for watching just Less files and automatically building them whenever you save.

### Troubleshooting dependencies

Should you encounter problems with installing dependencies or running Grunt commands, uninstall all previous dependency versions (global and local). Then, rerun `npm install`.


